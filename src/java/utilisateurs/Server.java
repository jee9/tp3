package utilisateurs;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilisateurs.modeles.User;
import utilisateurs.gestionnaires.UserHandler;

public class Server {

    /** Gestionnaire de {@link User} */
    static public UserHandler uh = new UserHandler();
    private static boolean initialized = false;

    public static void init(String ressourceDir) {
        if (!initialized) {
           // try {
                //  try {
                Loader.init(ressourceDir);
                Writer.init(ressourceDir);
                
                
               // Writer.serialize();
                Loader.load(); // Chargement des fichiers XML
                initialized = true;
                //    } catch (IOException ex) {
               
                // }
           // } catch (IOException ex) {
               
           // }

        }
    }
}
